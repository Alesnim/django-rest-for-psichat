import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import ru from 'vuetify/es5/locale/ru'

Vue.use(Vuetify, {
  theme:{
    primary: '#009688',
    secondary: '#4caf50',
    accent: '#607d8b',
    error: '#f44336',
    warning: '#ff9800',
    info: '#2196f3',
    success: '#ffc107'
    },
  customProperties: false,
  iconfont: 'md',
  lang: {
    locales: { ru },
    current: 'ru'
  },
})
