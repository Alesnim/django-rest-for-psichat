import Vue from 'vue'
import Router from 'vue-router'
import MainPage from './components/MainPage.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'mainPage',
            component: MainPage
        },
        {
            path: '/admin',
            name: 'admin',
            component: () => import('./components/Admin.vue'),
            children: [
              {
                path: '/admin/scenarios',
                name: 'scenarios',
                component: () => import('./components/Scenarios/ListScenary.vue')
              },
              {
                path: '/admin/keywords',
                name: 'keywords',
                component: () => import('./components/Keywords/ListKeywords.vue')
              },
              {
                path: '/admin/chats',
                name: 'chats',
                component: () => import('./components/Chats/ListChats.vue')
              },
              {
                path: '/admin/replics',
                name: 'replics',
                component: () => import('./components/Replics/ListReplics.vue')
              },
              {
                path: '/admin/lastMessages',
                name: 'lastMessages',
                component: () => import('./components/LastMessages/ListLastMessages.vue')
              },
            ]
        },
    ]
})
