import Vue from 'vue'
import Vuex from 'vuex'
import ip_address from './ip_address'
const axios = require('axios');
const moment = require('moment');

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    socket: {
      isConnected: false,
      message: '',
      reconnectError: false,
      messagesList: []
    },
    auth: {
      authorized: localStorage.authorized,
      token: localStorage.token,
      username: localStorage.username,
      id: ''
    },
    admin: {
      chats: [],
      dialogs: [],
      keywords: [],
      lastMessages: [],
      replics: [],
      newDialog: {
        name: '',
        description: '',
        number_steps: '',
        key_word: ''
      }
    },
  },
  getters: {
    newDialog: state => state.newDialog,
    dialogs: state => state.admin.dialogs,
    chats: state => state.admin.chats,
    lastMessages: state => state.admin.lastMessages,
    keywords: state => state.admin.keywords
  },
  mutations: {
    SOCKET_ONOPEN(state, event) {
      Vue.prototype.$socket = event.currentTarget
      state.socket.isConnected = true
    },
    SOCKET_ONCLOSE(state, event) {
      state.socket.isConnected = false
    },
    SOCKET_ONERROR(state, event) {
      console.error(state, event)
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE(state, message) {
      console.log(message)
      state.socket.message = message
      state.socket.messagesList.push({
        author: message.author,
        content: message.content
      })
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {
      console.info(state, count)
    },
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
    MESSAGE_TO_WEBSOCKET(state, message) {
      state.message = message;
    },
    LOGIN_TOKEN(state, payload) {
      ;
      state.auth.token = payload.token;
      localStorage.token = payload.token;

      state.auth.authorized = true;
      localStorage.authorized = true;
    },
    SET_USERNAME_BLYAT(state, payload) {
      //TODO: NOT BLYAT
      state.auth.username = payload;
      localStorage.username = payload;
    },
    CLEAR_DATA(state, payload) {
      state.auth.username = '';
      state.auth.token = '';
      state.auth.authorized = false;

      localStorage.username = '';
      localStorage.token = '';
      localStorage.authorized = false;
    },
    SET_SCENARY(state, dialogs) {
      state.dialogs = dialogs
    },
    SET_DIALOGS(state, data) {
      state.admin.dialogs = data.data
    },
    SET_KEYWORDS(state, data) {
      state.admin.keywords = data
    },
    SET_REPLICS(state, data) {
        data.forEach((el) => {
          var kwId = el.dialogue.match(/(?:)\w+$/g)[0]
          var dialog = state.admin.dialogs.find((el, index, array) => {
            if (el.id == kwId) {
              return true
            }
            return false
          })
          if (dialog['replics'] == null) {
            dialog['replics'] = []
          }
          dialog['replics'].push({text: el.text})
        })
    },
    SET_LAST_MESSAGES(state, data) {
      for(var counter = 0;counter < data.length; counter++) {
        var date = data[counter].date
        data[counter].date = moment(date).format("DD MMM HH:mm:ss")
      }
      state.admin.lastMessages = data
    },
    ADD_SCENARY(state, dialogObject) {
      state.dialogs.push(dialogObject)
    },
    CLEAR_NEW_SCENARY(state) {
      state.newDialog = {
        name: '',
        description: '',
        number_steps: '',
        key_word: ''
      }
    }

  },
  actions: {
    sendMessage: function(context, message) {
      Vue.prototype.$socket.send(message);
      context.state.socket.messagesList.push({
        author: context.state.auth.username,
        content: message
      })
    },
    login: function(context, data) {
      axios({
        method: 'post',
        url: 'http://' + ip_address() + '/auth/token/create/',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        data: data
      }).then(response => {
        context.commit('LOGIN_TOKEN', {
          token: response.data.auth_token
        })
        context.state.auth.authorized = true;
        localStorage.authorized = true;
      })
    },

    logout: function(context, data) {
      context.commit('CLEAR_DATA')
      axios({
        method: 'post',
        url: 'http://' + ip_address() + '/auth/token/destroy/',
        headers: {
          'Authorization': 'Token ' + context.state.auth.token
        }
      })
    },
    loadChats: function(context, data) {
      axios({
        method: 'get',
        url: 'http://' + ip_address() + '/api/chat/',
        headers: {
          'Authorization': 'Token ' + context.state.auth.token
        }
      }).then((response) => {
        context.commit('SET_DIALOGS', response.data)
      })
    },
    loadLastMessages: function(context, data) {
      axios({
        method: 'get',
        url: 'http://' + ip_address() + '/api/message/',
        headers: {
          'Authorization': 'Token ' + context.state.auth.token
        }
      }).then((response) => {
        context.commit('SET_LAST_MESSAGES', response.data)
      })
    },
    loadDialogs: function(context, data) {
      axios({
        method: 'get',
        url: 'http://' + ip_address() + '/api/dialogue/',
        headers: {
          'Authorization': 'Token ' + context.state.auth.token
        }
      }).then((response) => {
        context.dispatch('loadReplics')
        context.commit('SET_DIALOGS', response)
      })
    },
    loadKeywords: function(context, data) {
      axios({
        method: 'get',
        url: 'http://' + ip_address() + '/api/keyword/',
        headers: {
          'Authorization': 'Token ' + context.state.auth.token
        }
      }).then((response) => {
        context.commit('SET_KEYWORDS', response.data)
      })
    },
    loadReplics: function(context, data) {
      axios({
        method: 'get',
        url: 'http://' + ip_address() + '/api/replica/',
        headers: {
          'Authorization': 'Token ' + context.state.auth.token
        }
      }).then((response) => {
        context.commit('SET_REPLICS', response.data)
      })
    },
    saveScenary: function(context, data) {
      axios({
        method: 'post',
        url: 'http://' + ip_address() + '/api/dialogue/',
        headers: {
          'Authorization': 'Token ' + context.state.auth.token
        },
        data: data
      })
    },
    addDialog({
      commit,
      state
    }) {

      const todo = {
        name: state.newDialog.name,
        description: state.newDialog.description,
        number_steps: state.newDialog.number_steps,
        key_word: state.newDialog.key_word
      };
      axios({
        method: 'post',
        url: 'http://' + ip_address() + '/api/dialogue/',
        headers: {
          'Authorization': 'Token' + context.state.auth.token
        }
      }).then(_ => {
        commit('ADD_DIALOG', todo)
      })
    },
    clearNewTodo({
      commit
    }) {
      commit('CLEAR_NEW_DIALOG')
    },
  }
})
