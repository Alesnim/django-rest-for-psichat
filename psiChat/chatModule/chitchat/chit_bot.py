import sys
import os
import signal
import platform
import curses
import time


PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))





from psiChat.chatModule.chitchat.Preparation import Preparation
from psiChat.chatModule.chitchat.w2v_utils import CoderW2V
from psiChat.chatModule.chitchat.Training_net import Training
from psiChat.chatModule.chitchat.prediction import Prediction



f_source_data = 'data/source_data.txt'
f_prepared_data = 'data/prepared_data.txt'
f_prepared_data_pkl = 'data/prepared_data.pkl'
f_encoded_data = 'data/encoded_data.npz'
f_w2v_model = 'data/w2v_model.bin'
f_w2v_vocab = 'data/w2v_vocabulary.txt'
f_w2v_nbhd = 'data/w2v_neighborhood.txt'
f_net_model = 'data/net_model.txt'
f_net_weights = 'data/net_final_weights.h5'


def train():
    start_time = time.time()
    prep = Preparation()
    prep.prepare_all(f_source_data, f_prepared_data)

    w2v = CoderW2V()
    w2v.words2vec(f_prepared_data_pkl, f_encoded_data, f_w2v_model, f_w2v_vocab, f_w2v_nbhd, size=500, epochs=1000,
                  window=5)

    t = Training()
    t.train(f_encoded_data, f_net_model, 2, 100, 5)

    pr = Prediction(f_net_model, f_net_weights, f_w2v_model)
    pr.assessment_training_accuracy(f_encoded_data)

    print('\n[i] Общее время обучения: %.4f мин или %.4f ч' % (
    (time.time() - start_time) / 60.0, ((time.time() - start_time) / 60.0) / 60.0))

    building_language_model(f_source_data)


def predict(for_speech_recognition=False, for_speech_synthesis=False):
    ''' Работа с обученной моделью seq2seq.
    1. for_speech_recognition - включение распознавания речи с микрофона с помощью PocketSphinx
    2. for_speech_synthesis - включение озвучивания ответов с помощью RHVoice '''

    pr = Prediction(f_net_model, f_net_weights, f_w2v_model)

    if for_speech_recognition:
        print('[i] Инициализация языковой модели...')
        sr = SpeechRecognition('from_microphone')

    print('\n')
    quest = ''
    while (True):
        if for_speech_recognition == True:
            print('Слушаю...')
            quest = sr.stt()
            os.write(sys.stdout.fileno(), curses.tigetstr('cuu1'))
            print('Пользователь: ' + quest)
        else:
            quest = input("Пользователь: ")
        answer = pr.predict(quest)
        print("\t=> %s\n" % answer)
        if for_speech_synthesis == True:
            tts(answer, 'playback')


# Попробовать в качестве центральной сети использовать pynlc или chatterbot (что бы была поддержка сценария и некоторого контекста, который можно
# брать из БД)

def main():
    curses.setupterm()
    if len(sys.argv) > 1:
        if sys.argv[1] == 'train':
            train()
        elif sys.argv[1] == 'predict':
            if len(sys.argv) > 2:
                if sys.argv[2] == '-ss':
                    if len(sys.argv) > 3:
                        if sys.argv[3] == '-sr':
                            predict(True, True)
                        else:
                            print("[E] Неверный аргумент командной строки '" + sys.argv[
                                3] + "'. Введите help для помощи.")
                            sys.exit(0)
                    predict(False, True)

                elif sys.argv[2] == '-sr':
                    if len(sys.argv) > 3:
                        if sys.argv[3] == '-ss':
                            predict(True, True)
                        else:
                            print("[E] Неверный аргумент командной строки '" + sys.argv[
                                3] + "'. Введите help для помощи.")
                            sys.exit(0)
                    predict(True, False)

                else:
                    print("[E] Неверный аргумент командной строки '" + sys.argv[2] + "'. Введите help для помощи.")
                    sys.exit(0)
            predict()
        elif sys.argv[1] == 'help':
            print('Поддерживаемые варианты работы:')
            print('\ttrain - обучение модели seq2seq')
            print('\tpredict - работа с обученной моделью seq2seq')
            sys.exit(0)
        else:
            print("[E] Неверный аргумент командной строки '" + sys.argv[1] + "'. Введите help для помощи.")
            sys.exit(0)
    else:
        print('[i] Выберите вариант работы бота:')
        print('\t1. train - обучение модели seq2seq')
        print('\t2. predict - работа с обученной моделью seq2seq')
        while True:
            choice = input('Введите цифру: ')
            if choice == '1':
                choice = input('Вы уверены?(д/н) ')
                if choice == 'д' or choice == 'y':
                    train()
                break
            elif choice == '2':
                predict()
                break
            elif choice == '3':
                predict(False, True)
                break
            elif choice == '4':
                predict(True, False)
                break
            elif choice == '5':
                predict(True, True)
                break
            else:
                os.write(sys.stdout.fileno(), curses.tigetstr('cuu1'))


def on_stop(*args):
    print("\n[i] Бот остановлен")
    sys.exit(0)


if __name__ == '__main__':
    # При нажатии комбинаций Ctrl+Z, Ctrl+C либо закрытии терминала будет вызываться функция on_stop()
    if platform.system() == "Linux":
        for sig in (signal.SIGTSTP, signal.SIGINT, signal.SIGTERM):
            signal.signal(sig, on_stop)
    main()