"""
Основной метод работы с поиском шаблонов
"""

# TODO:
# 1. Основная петля []
# 2. Поиск паттерна []
# 3. Вызов читчата []
#
#
#
#
from numpy import random
import nltk
from pymystem3 import Mystem
from string import punctuation
from psiChat.models import KeywordsOld, Dialogue, Replica


class PatternMatching():
    def __init__(self):
        self.intent = 1
        self.sent_text_num = 0
        self.sent_text = []
        self.get_text_num = 0
        self.get_text = []
        self.russian_stop = nltk.corpus.stopwords.words('russian')

    @staticmethod
    def hello():
        hello = ['Здравствуй', "Привет", "Добрый день"]
        return str(hello[random.randint(0, 2)])

    def preprocess_text(self, text):
        mystem = Mystem()
        tokens = mystem.lemmatize(text.lower())
        tokens = [token for token in tokens if token not in self.russian_stop \
                  and token != " " \
                  and token.strip() not in punctuation]
        return tokens

    def predict(self, text, number):
        text = self.preprocess_text(text)
        test_list = KeywordsOld.objects.all().values_list('word')
        for token in text:
            if token in test_list[0]:
                return 'Ключевое слово найдено. Ключевое слово {}'.format(token)
            else:
                answer = False
                return answer


def get_input(terminal=1, inp=None):
    if terminal:
        raw_input = input()
        return raw_input
    else:
        raw_input = inp
        return inp

# def get_sent(inp, terminal=1):
# import pattern_chat.patterns as pattern
# import pattern_chat.scenary_files as scenary
# inp = pattern.get_clear_sentence(inp)
# inp = pattern.search(inp, scenary.KeyScenary)
# out = scenary.KeyScenary.get_scenario_name(inp)
# out = scenary.Scenario(key_word=out)
# out = scenary.Scenario.get_step(scenary=out, step=0, phase=0)
# if terminal:
# return print(out)
# else:
# return out


# if __name__ == '__main__':
# hello()
# if input() != "выход":
# get_sent(get_input())
