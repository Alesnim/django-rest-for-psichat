"""
Работает с файлами сценариев
"""
import pandas


# TODO:
# 1. Класс индекса сценариев с ключевыми словами [Y]
# 1.1. Метод инит [Y]
# 1.2. Чтение индексов - возвращение в виде списка [Y]
# 1.3. Чтение ключевых слов - возвращение в виде списка [Y]
# 2. Класс работы со сценарием [Y]
# 2.1. Метод инит [Y]
# 2.2. Загрузка из csv [Y]
# 2.3. Получение всех реплик по ключу сценария [Y]
# 2.4. Получение шага из сценария [Y]
#
#
#

class KeyScenary:
    """
    Класс отвечающий за поиск ключевых слов и сопоставление сценарию
    """

    def __init__(self, path_to_index="./data/key_word_to_index.csv") -> object:
        self.file = open(path_to_index, 'r')
        self.path = path_to_index
        self.index_name = self.read_index(path_to_index=self.path)
        self.values = self.__read_word__(path_to_index=self.path)
        self.reader = self.reader_csv(path_to_index)

    def __csv_dict_writer__(self, fieldnames, data):
        """
        Writes a CSV file using DictWriter
        """
        import csv
        with open(self.path, "w", newline='') as out_file:
            writer = csv.DictWriter(out_file, delimiter=',', fieldnames=fieldnames)
            writer.writeheader()
            for row in data:
                writer.writerow(row)

    def read_index(self, path_to_index=None):
        reader = pandas.read_csv(path_to_index, sep=",")
        keys = reader.keys()
        return keys

    def __read_word__(self, path_to_index=None):
        """

        :param path_to_index: str
        :return:
        """
        reader = pandas.read_csv(path_to_index, sep=",")
        return reader.values

    def get_scenario_name(self, word: str) -> str:
        """
        Ищет название сценария по ключевому слову
        :param word:
        :return: name of scenario (str)
        """
        reader = self.reader_csv(self.path)
        df = pandas.DataFrame(reader, columns=list(self.index_name))
        if word in self.values:
            for name in df:
                for words in df[name]:
                    if words == word:
                        return name

    def reader_csv(self, path):
        return pandas.read_csv(path, sep=",")


class Scenario:
    """
    Класс для работы с файлами csv содержащими сценарии ответных реплик

    """

    def __init__(self, path_to_scenario='./data/scenario.csv', key_word=None):
        self.path = path_to_scenario
        self.reader()
        self.sentence_return(key_word=key_word)
        self.key_word = key_word

    def reader(self):
        """
        Читаем датафрейм из словаря
        :return: data_frame
        """
        data_frame = pandas.read_csv(self.path, sep=",")
        data_frame = pandas.DataFrame(data_frame, columns=list(data_frame.keys()))
        data_frame.index = [k for k in data_frame["Name"]]
        return data_frame

    def sentence_return(self, key_word):
        """
        Забираем сценарий по ключевому слову
        :return:
        :param key_word:
        """
        data = self.reader()
        scenario = None
        if key_word is not None:
            scenario = data.loc[key_word]
        return scenario

    def get_step(self, scenary, step, phase):
        """
        Возвращает реплику по шагу и фазе

        :type scenary: dataframe
        """
        scenary = scenary[scenary.Step == step]
        scenary = scenary[scenary.Phase == phase]
        scenary = scenary.Request.astype(str)
        scenary = scenary[0]
        return scenary
