import os
from sentimental import Sentimental


class PredictMessageClass:

    def PredictMessage(self, message):
        sent = Sentimental()
        score = sent(message)['score']
        return score

    def PredictLongMessage(self, message):
        return self.sent(message)['comparative']


