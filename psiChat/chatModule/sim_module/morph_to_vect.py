import gensim
import pymorphy2
import re


# TODO:
#
#
#
#

def load_model(path_to_model: str, binary: bool = True) -> object:
    return gensim.models.KeyedVectors.load_word2vec_format(path_to_model, binary)


def sims(model, init_sims=True):
    model.init_sims(replace=init_sims)


def init_morph() -> object:
    return pymorphy2.MorphAnalyzer()


# pymorphy2: word2vec
cotags = {'ADJF': 'ADJ',
          'ADJS': 'ADJ',
          'ADVB': 'ADV',
          'COMP': 'ADV',
          'GRND': 'VERB',
          'INFN': 'VERB',
          'NOUN': 'NOUN',
          'PRED': 'ADV',
          'PRTF': 'ADJ',
          'PRTS': 'VERB',
          'VERB': 'VERB'}


def search_neighbors_2(word, pos, gend='masc', model=None, morph=None):
    if word in model:
        neighb = model.most_similar([word], topn=20)
        for neig in neighb:
            word_n = neig[0]
            pos_n = morph.parse(neig[0])[0].tag.POS
            if pos == pos_n:
                if pos == 'NOUN':
                    parse_res = morph.parse(word_n)
                    for ana in parse_res:
                        if ana.normal_form == word_n:
                            if ana.tag.gender == gend:
                                return word_n
                elif pos == 'VERB' and word[-2:] == 'ся':
                    if word_n[-2:] == 'ся':
                        return word_n
                elif pos == 'VERB' and word[-2:] != 'ся':
                    if word_n[-2:] != 'ся':
                        return word_n
                else:
                    return word_n
    return word


def get_simul(model, word):
    return model.most_simular(word, topn=10)
