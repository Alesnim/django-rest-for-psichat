import re
import nltk.tokenize as tokenize
"""
Методы обработки сырого входного текста
"""

def get_clear_lines(lines: str, replace: bool = True) -> str:
    """
    Очищаем строки от непечатных символов

    :param lines: str
    :param replace:
    :return: clear_line(str)
    """
    if replace:
        lines = re.sub(r'[^А-Яа-я.,!?:\-\s0-9]*', '', lines)
        lines.lower()
    return lines


def tokenize_line(clear_line: str) -> list:
    """
    Токенизируем входящую строку при помощи nltk
    :param clear_line: str
    :return: token_list (list)
    """
    tokenize_sent = tokenize.word_tokenize(clear_line)
    return tokenize_sent


def tokenize_sentence(lines: str) -> list:
    """
    Разбиваем текст на предложения
    :param lines: str
    :return: list of str
    """
    return lines.split('.')
