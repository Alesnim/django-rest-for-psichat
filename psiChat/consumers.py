import time

from channels.generic.websocket import WebsocketConsumer
import json
from django.conf import settings
from psiChat.chatModule.pattern_chat.chat import PatternMatching
from .models import Message, Chat

from rest_for_psichat.settings import graph


def predict(text):
    with graph.as_default():
        pr = settings.MODEL
        answer = pr.predict(text)
    return answer


def pattern_predict(text, pattern):
    answer = pattern.predict(text, pattern.intent)
    if answer:
        pattern.intent += 1
    if isinstance(answer, tuple) and answer[0] == 'END':
        pattern.intent = 1
        return answer[1]
    return answer




class SocketWeb(WebsocketConsumer):

    def connect(self):
        self.accept()
        self.send(json.dumps({"content": "Добрый день! ", "author": "BOT"}))
        self.chat = Chat.objects.create(name=time.strftime("%Y-%m-%d-%H.%M.%S", time.localtime()))
        self.pattern = PatternMatching()

    def receive(self, text_data):
        text = text_data
        Message.objects.create(id_chat=self.chat, text=text, id_user='test')
        text = pattern_predict(text, pattern=self.pattern)
        if text == False:
            text = predict(text_data)
            Message.objects.create(id_chat=self.chat, text=text, id_user='bot')
        text = json.dumps({"content": text, "author": "BOT"})
        if text:
            # text = json.dumps({"content": text, "author": "BOT"})
            self.send(text)
