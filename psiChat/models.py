import json

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.


class Dialogue(models.Model):

    """
    Модель преставляющая редактируемый диалог
    """

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    number_steps = models.IntegerField(default=0)
    keywords = models.TextField()


    def set_keywords(self, x):
        self.keywords = json.dump(x)


    def get_keywords(self):
        return json.loads(self.keywords)

    def __str__(self):
        return self.name


class KeywordsOld(models.Model):
    id_dialogue = models.ForeignKey(Dialogue, on_delete=models.CASCADE)
    word = models.CharField(max_length=80)

    def __str__(self):
        return str(self.word)


class Replica(models.Model):
    """
    Модель представляющая редактируемую реплику в диалоге
    """

    dialogue = models.ForeignKey(Dialogue, on_delete=models.CASCADE)
    text = models.TextField(verbose_name="text_replic")
    number_sent = models.IntegerField()

    def __str__(self):
        return self.text


class Chat(models.Model):
    """
    Класс экземпляра чата
    """

    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Message(models.Model):
    """
    Класс экземпляра  сообщения в чате
    """

    id_chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    id_user = models.CharField(max_length=80)

    def __str__(self):
        return str(self.id)


class MessageLog(models.Model):
    """
    Класс сохраненного в истории сообщения
    """

    id_chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)


class SentimentAnalis(models.Model):
    """
    Класс результатов анализа сообщений Достоевским
    """

    id_message = models.OneToOneField(Message, on_delete=models.CASCADE)
    result = models.CharField(max_length=100)


@receiver(post_save, sender=Message)
def save_sentiment_analisys_message(sender, instance, created, **kwargs):
    from psiChat.chatModule.sentiment.sentimentMessage import PredictMessageClass
    from sentimental import Sentimental

    sent = Sentimental()
    pr = PredictMessageClass()
    if created:
        SentimentAnalis.objects.create(
            id_message=instance, result=sent.analyze(instance.text)["score"]
        )
