from rest_framework import viewsets

from psiChat.models import Chat
from psiChat.serializers import ChatSerializer


class ChatViewSet(viewsets.ModelViewSet):
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer

