from rest_framework import viewsets
import rest_framework.permissions as permissions
from psiChat.models import Dialogue, KeywordsOld, Replica
from psiChat.serializers import DialogueSerializer
from rest_framework.response import Response
import json


class DialogueViewSet(viewsets.ModelViewSet):
    queryset = Dialogue.objects.all()
    serializer_class = DialogueSerializer
    permissions_classes = [permissions.AllowAny]

    def get_queryset(self):
        """
        This view should return a list of all records
        """
        return Dialogue.objects.all().order_by('name')

    def create(self, request):
        """
        create dialogue model
        create keywords with this dialogue
        create steps
        """
        data = request.POST

        createdDialog = Dialogue.objects.create(name=data['name'], description=data['description'], number_steps=data['number_steps'])
        keywords = json.loads(data['keywords'])
        for kw in keywords:
            createdKw = KeywordsOld.objects.create(id_dialogue=createdDialog, word=kw['name'])

        steps = json.loads(data['steps'])
        for step in steps:
            createdStep = Replica.objects.create(dialogue=createdDialog, text=step['name'], number_sent=step['number_sent'])

        return Response({"status": "ok"})
