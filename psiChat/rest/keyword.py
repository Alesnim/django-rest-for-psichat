from rest_framework import viewsets

import rest_framework.permissions as permissions
from psiChat.models import KeywordsOld
from psiChat.serializers import KeywordsSerializer
from rest_framework.response import Response


class KeywordsViewSet(viewsets.ModelViewSet):
    queryset = KeywordsOld.objects.all()
    serializer_class = KeywordsSerializer
    permissions_classes = [permissions.AllowAny]

    def get_queryset(self):
        """
        This view should return a list of all records
        """
        return KeywordsOld.objects.all()
