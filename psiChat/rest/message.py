from rest_framework import viewsets
from rest_framework.response import Response

from psiChat.models import Message, SentimentAnalis
from psiChat.serializers import MessageSerializer, SentimentAnalisSerializer

class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def list(self, request):
        queryset = Message.objects.all()
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

class SentimentAnalisSet(viewsets.ModelViewSet):
    queryset = SentimentAnalis.objects.all()
    serializer_class = SentimentAnalisSerializer

    def list(self, request, pk):
        sent = SentimentAnalis.objects.get(id_message=pk)
        serializer = SentimentAnalisSerializer(sent)

        return Response(serializer.data)
