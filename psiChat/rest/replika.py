from rest_framework import viewsets
import rest_framework.permissions as permissions
from psiChat.models import Replica
from psiChat.serializers.ReplicaSerializer import ReplicaSerializer

class ReplicaViewSet(viewsets.ModelViewSet):
    queryset = Replica.objects.all()
    serializer_class = ReplicaSerializer
    permissions_classes = [permissions.AllowAny]

    def get_queryset(self):
        """
        This view should return a list of all records
        """
        return Replica.objects.all()
