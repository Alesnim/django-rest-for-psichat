from django.conf.urls import url

from . import consumers

websocket_url_patterns = [
    url(r'^ws/', consumers.SocketWeb)
]