from rest_framework import serializers

from psiChat.models import Dialogue


class DialogueSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Dialogue
        fields = ('id','name', 'description', 'keywords', 'number_steps',)
