from djoser.urls.base import User
from rest_framework import serializers

from psiChat.models import Message, Chat, SentimentAnalis


class MessageSerializer(serializers.ModelSerializer):
   # id_user = serializers.SlugRelatedField(many=False, slug_field='id', queryset=User.objects.all())
   # id_chat = serializers.SlugRelatedField(many=False, slug_field='id', queryset=Chat.objects.all())
    class Meta:
        model = Message
        fields = ( 'id', 'id_user','date', 'text', 'id_chat')
