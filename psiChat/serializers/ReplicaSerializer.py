from rest_framework import serializers

from psiChat.models import Replica


class ReplicaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Replica
        fields = ('dialogue', 'text',)
