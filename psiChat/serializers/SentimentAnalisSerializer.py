from djoser.urls.base import User
from rest_framework import serializers

from psiChat.models import SentimentAnalis

class SentimentAnalisSerializer(serializers.ModelSerializer):
    class Meta:
        model = SentimentAnalis
        fields = ( '__all__' )
