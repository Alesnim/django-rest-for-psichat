from django.test import TestCase, Client
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from psiChat.models import Dialogue


class DialogueCreateTest(APITestCase):
    def setUp(self):
        self.Test2 = Dialogue.objects.create(name='Test2', description='test test test',)

    def test_dialogue_list(self):
        from psiChat.serializers import DialogueSerializer
        responce = self.client.get('/api/dialogue')
        dialog = Dialogue.objects.all()
        serialize = DialogueSerializer(dialog, many=True)
        self.assertEqual(responce.data, serialize.data)
        self.assertEqual(responce.status_code, status.HTTP_200_OK)

    def test_dialogue2(self):
        from psiChat.serializers import DialogueSerializer
        response = self.client.get('/api/dialogue/1')
        dialog = Dialogue.objects.get(id=1)
        serialize = DialogueSerializer(dialog)
        self.assertEqual(response.data, serialize.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

