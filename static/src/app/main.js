import Vue from 'vue';
import VueRouter from 'vue-router';
import scenary from './view/scenary.vue';

 const route = [
    {path:"/", component: scenary},

];
Vue.use(VueRouter);
const router = new VueRouter({
    routes: route,
});


new Vue({
    el: '#app',
    router: router
});

