'use strict';
const webpack = require('webpack');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const config = {
  watch: true,
  mode: 'development',
  entry: './app/main.js',
  output: {
    filename: './app.js'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.less$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'less-loader'
        ]
      },
      {
        test:/\.css$/,
        use: ['css-loader', 'less-loader']
      }
    ]
  },
  resolve: {
    alias: {
       'vue': 'vue/dist/vue.js'
    },
    extensions: [
      '.js',
      '.vue'
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
  ]
}

module.exports = config;